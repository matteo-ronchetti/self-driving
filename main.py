import argparse
import logging
import random
import time

import numpy as np
import cv2

from carla.client import make_carla_client
from carla.sensor import Camera, Lidar
from carla.settings import CarlaSettings
from carla.tcp import TCPConnectionError
from carla.util import print_over_same_line


def print_measurements(measurements):
    number_of_agents = len(measurements.non_player_agents)
    player_measurements = measurements.player_measurements
    message = 'Vehicle at ({pos_x:.1f}, {pos_y:.1f}), '
    message += '{speed:.0f} km/h, '
    message += 'Collision: {{vehicles={col_cars:.0f}, pedestrians={col_ped:.0f}, other={col_other:.0f}}}, '
    message += '{other_lane:.0f}% other lane, {offroad:.0f}% off-road, '
    message += '({agents_num:d} non-player agents in the scene)'
    message = message.format(
        pos_x=player_measurements.transform.location.x,
        pos_y=player_measurements.transform.location.y,
        speed=player_measurements.forward_speed * 3.6,  # m/s -> km/h
        col_cars=player_measurements.collision_vehicles,
        col_ped=player_measurements.collision_pedestrians,
        col_other=player_measurements.collision_other,
        other_lane=100 * player_measurements.intersection_otherlane,
        offroad=100 * player_measurements.intersection_offroad,
        agents_num=number_of_agents)
    print(message)


host = "34.243.41.251"
port = 2000

with make_carla_client(host, port) as client:
    settings = CarlaSettings()
    settings.set(
        SynchronousMode=True,
        SendNonPlayerAgentsInfo=True,
        NumberOfVehicles=20,
        NumberOfPedestrians=40,
        WeatherId=1,
        QualityLevel='Epic')
    settings.randomize_seeds()

    # The default camera captures RGB images of the scene.
    camera0 = Camera('CameraRGB')
    camera0.set_image_size(400, 300)
    camera0.set_position(0.30, 0, 1.30)
    settings.add_sensor(camera0)

    camera0 = Camera('CameraDepth', PostProcessing='Depth')
    camera0.set_image_size(400, 300)
    camera0.set_position(0.30, 0, 1.30)
    settings.add_sensor(camera0)

    scene = client.load_settings(settings)

    # Choose one player start at random.
    number_of_player_starts = len(scene.player_start_spots)
    player_start = random.randint(0, max(0, number_of_player_starts - 1))

    # Notify the server that we want to start the episode at the
    # player_start index. This function blocks until the server is ready
    # to start the episode.
    print('Starting new episode...')
    client.start_episode(player_start)

    s = time.time()
    for frame in range(0, 20000):
        # Read the data produced by the server this frame.
        measurements, sensor_data = client.read_data()

        print_measurements(measurements)

        for name, measurement in sensor_data.items():
            print(name, measurement)

            if name == "CameraRGB":
                array = np.frombuffer(measurement.raw_data, dtype=np.dtype("uint8"))
                array = np.reshape(array, (measurement.height, measurement.width, 4))
                array = array[:, :, :3]
                cv2.imshow("camera", array)
            else:
                array = np.frombuffer(measurement.raw_data, dtype=np.dtype("uint8"))
                array = np.reshape(array, (measurement.height, measurement.width, 4)).astype(np.float32)
                normalized = np.log(1 + (array[:, :, 2] + array[:, :, 1] * 256 + array[:, :, 0] * 256 * 256) * 1000.0 / (
                            256 * 256 * 256 - 1))
                normalized /= np.max(normalized)

                cv2.imshow("depth", (normalized * 255).astype(np.uint8))

        cv2.waitKey(10)

        control = measurements.player_measurements.autopilot_control
        client.send_control(control)

        print("FPS", 1/(time.time() - s))
        s = time.time()
